import React from 'react';
import PropTypes from 'prop-types';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';

function TransactionItem(props) {
  const {	transaction } = props;
  
  // Format date
  const dt = new Date(transaction.effectiveDate);
  const dateFormatted = `${
      (dt.getMonth()+1).toString().padStart(2, '0')}/${
      dt.getDate().toString().padStart(2, '0')}/${
      dt.getFullYear().toString().padStart(4, '0')} ${
      dt.getHours().toString().padStart(2, '0')}:${
      dt.getMinutes().toString().padStart(2, '0')}:${
      dt.getSeconds().toString().padStart(2, '0')}`

	return (
    <Card>
      <Accordion.Toggle as={Card.Header}
        eventKey={transaction.id}
        style={{ 
          cursor: 'pointer',
          borderLeft: '10px solid',
          borderLeftColor: transaction.type === 'debit' ? 'red' : 'green'
        }}
      >
        <span
          style={{color: transaction.type === 'debit' ? 'red' : 'green'}}
        >
          $ {transaction.amount}
        </span>
        <span class="float-right">$ {transaction.balance}</span>
      </Accordion.Toggle>
      <Accordion.Collapse
        eventKey={transaction.id}
        style={{ borderLeft: '10px solid', borderLeftColor: transaction.type === 'debit' ? 'red' : 'green'}}>
        <Col  >
          <ul className="mt-2" style={{color: '#777' }}>
            <li><strong>ID:</strong> {transaction.id}</li>
            <li><strong>Type: </strong>{transaction.type}</li>          
            <li><strong>Date: </strong>{dateFormatted}</li>
            <li><strong>Balance result: </strong> $ {transaction.balance}</li>
          </ul>
        </Col>
      </Accordion.Collapse>
    </Card>
	);
}

TransactionItem.propTypes = {
	transaction: PropTypes.object.isRequired
};

TransactionItem.defaultProps = {
  transaction: {}
};

export default TransactionItem;
