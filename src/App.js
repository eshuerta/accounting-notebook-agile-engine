
import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Col, Row, Container, Accordion } from "react-bootstrap";
import TransactionItem from './components/transaction';
import './App.css';

function Account() {
 
  const [transactionList, setTransactionList] = useState([]);
  const [currentBalance, setCurrentBalance] = useState([]);
  useEffect(() => {
    setInterval(()=> {
      axios.get('/transaction')
      .then(response => {
        if (response && response.data && response.data.transactions)
          setTransactionList(response.data.transactions.length > 0 ? response.data.transactions : []);
          setCurrentBalance(response.data.balance);
      })
    }, 2000); // Continuously update the table with the new transactions, every 2 sec
  }, []);

  return (
    <Container>
      <Row className="mt-5">
        <Col md={{ span: 8, offset: 2 }}>
          <header>
            <h1>
              Account total:
              <span style={{ color: currentBalance < 1000 ? currentBalance < 100 ? 'red' : 'orange' : 'green'}} class="float-right">$ {currentBalance} </span>
            </h1>
          </header>          
        </Col>
      </Row>
      <Row>
        <Col>
          <hr />
        </Col>
      </Row>
      <Row className="mt-3">
        <Col md={{ span: 6, offset: 3 }}>
          <Row>
            <Col md="6">Movements</Col>
            <Col md="6" className="text-right">Total</Col>
          </Row>            
          <Accordion defaultActiveKey="0">
            {transactionList.map(transaction => 
              <TransactionItem transaction={transaction} key={transaction.id}/>
            )}              
          </Accordion>
        </Col>
      </Row>
    </Container>
  );
}

export default Account;
