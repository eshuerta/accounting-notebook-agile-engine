const express = require('express');
const bodyParser = require('body-parser');
var locks = require('locks');

const app = express();
const port = 5000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

let balance = 10000;
let transactions = [
  {
    "id": "1",
    "type": "credit",
    "amount": 10,
    "effectiveDate": "2020-04-25T07:09:12.021Z",
    "balance": 10010
  },
  {
    "id": "2",
    "type": "debit",
    "amount": 10,
    "effectiveDate": "2020-04-25T07:09:12.021Z",
    "balance": 10000
  }
];
// Lock
var mutex = locks.createMutex();

// get list or specific transaction
app.get('/transaction', (req, res) => {
  
  // Check if requests are locked
  if (!mutex.isLocked) {
    let response;
    let status = 200;

    if (!Object.keys(req.query).length) { // No parameters, try to get list
      response = {transactions, balance};
    } else if (req.query.transactionId) { // Get by id
      let transaction = transactions.find(x => x.id === req.query.transactionId);  // Find by id
      if (transaction) {
        response = transaction;
      } else { // transaction not found
        status = 404;
        response = "Transaction not found";
      }
    } else {
      status = 400;
    }
  
    return res.status(status).send(response);    
  } else {
    // TODO: retry operation?
    return res.status(200).send("Transaction in progress, try again later");    
  }  
});

// Create new transaction
app.post('/transaction/', function (req, res) {
  // Check if requests are locked
  if (!mutex.isLocked) {
    mutex.lock(function () { // Lock requests
      let response;
      let status = 400;
      res.setHeader('Content-Type', 'application/json');
    
      let { type, amount } = req.body;
      // required validations
      if (!type || !amount) {
        response = "'Type' and 'amount' are mandatory.";
      } else {
        
        // type input validation
        if (type !== 'credit' && type !== 'debit') {
          response = "Type must be 'credit' or 'debit'.";
        } else  if (isNaN(amount)) {   // numeric validation
          response = "Amount must be a valid number.";
        } else if (amount < 1) {
          response = "Amount must be more than 0.";
        } else {

          let newBalance = parseFloat(balance);
          let parsedAmount = parseFloat(amount);

          if (type === "debit") {
            newBalance -= parsedAmount;
          } else {
            newBalance += parsedAmount;
          }
          
          if (newBalance < 0) {
            status = 400;
            response = 'Not enought money in the account.';
          } else {
            balance = Math.round(newBalance * 100) / 100;
            // push the new transaction
            transactions.push({
              id: Date.now(),
              type: type,
              amount: parsedAmount,
              effectiveDate: new Date(Date.now()),
              balance
            });
            status = 200;
            response = 'Transaction completed succesfully.';
          }
          
        }
      }

      // Unlock requests
      mutex.unlock();      
      return res.status(status).send(response);    
    });
  } else {
    // TODO: retry operation?
    return res.status(200).send("Transaction in progress, try again later"); 
  }
});


app.listen(port, () => console.log(`Listening on port ${port}`));