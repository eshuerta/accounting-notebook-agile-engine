Installation: 

git clone https://gitlab.com/eshuerta/accounting-notebook-agile-engine.git

cd accounting-notebook-agile-engine

npm install


Run server and client: yarn dev

It will run automatically on default web browser on http:localhost:3002.
The page will automatically check for changes on server and show all transactions and account balance



Postman curl examples for server:

GET ALL TRANSACTIONS

curl --location --request GET 'http://localhost:3002/transaction' \
--header 'Content-Type: application/json'


---------------------------------- 


GET TRANSACTION BY ID:

curl --location --request GET 'http://localhost:3002/transaction?transactionId=1' \
--header 'Content-Type: application/json'

----------------------------------


NEW CREDIT:

curl --location --request POST 'http://localhost:3002/transaction' \
--header 'Content-Type: application/json' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'amount=3000' \
--data-urlencode 'type=credit'


----------------------------------


NEW DEBIT :

curl --location --request POST 'http://localhost:3002/transaction' \
--header 'Content-Type: application/json' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'amount=2000' \
--data-urlencode 'type=debit'


----------------------------------



TRANSACTIONS WITH ERRORS:

curl --location --request POST 'http://localhost:3002/transaction' \
--header 'Content-Type: application/json' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'amount=2000' \
--data-urlencode 'type=aaaa'

curl --location --request POST 'http://localhost:3002/transaction' \
--header 'Content-Type: application/json' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'amount=sssss' \
--data-urlencode 'type=credit'

curl --location --request POST 'http://localhost:3002/transaction' \
--header 'Content-Type: application/json' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'type=credit'

